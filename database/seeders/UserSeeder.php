<?php 
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Traits\CanCreateSlug;
use App\Models\Forms\FormQuestionType;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'name' => 'Gene Ellorin',
                'email' => 'gene@thisishatch.com',
                'password' => \Illuminate\Support\Facades\Hash::make('secret')
            ],
            [
                'name' => 'Paula',
                'email' => 'paula@thisishatch.com',
                'password' => \Illuminate\Support\Facades\Hash::make('secret')
            ]
        ];

        foreach ($data as $item)
            \App\Models\User::create($item);
    }
}
