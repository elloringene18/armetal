@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/contact.css?v=<?php echo rand(1,999999); ?>">
    <link rel="stylesheet" href="{{ asset('') }}/css/structures.css?v=<?php echo rand(1,999999); ?>">
    <link rel="stylesheet" href="{{ asset('') }}/css/about.css?v=<?php echo rand(1,999999); ?>">
@endsection

@section('content')
    <section id="homebanner">
        <!--  <span class="lines slideleftright"></span>-->
        <!-- <img src="img/homebanner.png" width="100%"> -->

        <video width="100%" loop muted autoplay playsinline>
            <source src="{{ asset('') }}/img/about/3-opt.mp4" type="video/mp4">
            Your browser does not support the video tag.
        </video>
    </section>

    <section id="design">
        <div class="container top">
            <div class="row">
                <div class="col-md-9">
                    <h2>Armetal has been designing and manufacturing custom-made metals from its headquarters in Riyadh, Saudi Arabia, since 1985. </h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 relative">
                    <img src="{{ asset('') }}/img/about/steel2.png" width="100%" class="mt-5">
                    <img src="{{ asset('') }}/img/about/steel-arrows.png" width="100%" class="mt-5" id="arrows">
                </div>
                <div class="col-md-8 right">
                    <p>By working in close partnership with architects, designers, and developers, we have designed and constructed some of the Middle East’s most inspiring architectural projects and landmarks. Iconic Armetal developments include the extraordinary <b>King Abdullah Petroleum Studies and Research Center, Riyad’s iconic Kingdom Centre, the breathtaking Al Faisalia Towers, the stunning landmark of King Fahd Airport in Dammam and the iconic and imposing Clock Tower of Mecca</b>.</p>
                    <p>Our work also benefits from collaborations across government and private sectors that have seen us create elegant and memorable architectural metalworks structures for some of the region’s <b>leading universities, hospitals, shopping malls, and public spaces within the upcoming Urban Planning developments</b>. </p>
                    <p>
                        The common thread that runs through all we do is our belief that every project has to <b>embody the artistic vision of the customer, the designer, and the architect</b>: a collaborative approach to creating beautiful metal products that are engineered to illuminate and demonstrate the accomplishment of the challenge.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section id="mission">
        <div class="container" id="principles">
            <div class="row">
                <div class="col-lg-4 col-md-4">
                    <div class="bluebox">
                        <img src="{{ asset('') }}/img/about/integrity.png">
                        <h4>Integrity</h4>
                        <p>Every metalwork that is designed, crafted, and deployed by Armetal meets the artistic, commercial, and cultural objectives of all of its stakeholders.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="bluebox">
                        <img src="{{ asset('') }}/img/about/impact.png">
                        <h4>Impact</h4>
                        <p>By building trusted relationships, we can make our business sustainable, develop our people and make a meaningful contribution to people and communities.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4">
                    <div class="bluebox">
                        <img src="{{ asset('') }}/img/about/performance.png">
                        <h4>Performance</h4>
                        <p>We are driven by a belief in the importance of customer satisfaction, production excellence, creative freedom, and high performing technologies.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="projects">

                    <div class="tabs">
                        <div class="tab active">mission</div>
                        <div class="tab right">Vision</div>
                    </div>

                    <div class="tabcontents">
                        <div class="content active">
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <img src="{{ asset('') }}/img/about/target.png" width="100%">
                                </div>
                                <div class="col-md-9">
                                    <span>We set out to design, manufacture, and deliver bespoke, precision engineered metal works, sculptures, and structures that serve a commercial, cultural, aesthetic, and artistic purpose.</span>
                                </div>
                                <div class="col-md-1">
                                </div>
                            </div>
                        </div>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-1">
                                </div>
                                <div class="col-md-1">
                                    <img src="{{ asset('') }}/img/about/target.png" width="100%">
                                </div>
                                <div class="col-md-9">
                                    <span>To serve artists, companies, governments, and communities through the execution of structurally superior landmark projects that make a demonstrable and lasting contribution to society and the noble objectives of Saudi Vision 2030.</span>
                                </div>
                                <div class="col-md-1">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
    </section>
@endsection

@section('js')
