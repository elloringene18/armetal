@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/contact.css?v=<?php echo rand(1,999999); ?>">
@endsection

@section('content')
    <section id="design">
        <div class="imgtxt">
            <div class="row">
                <div class="col-md-4 left slide-in-left">
                    <img src="{{ asset('') }}/img/contact/jacks.png" class="img">
                </div>
                <div class="col-md-8 right slide-in-right">
                    <h1 class="heading">Let’s create</h1>
                    <p>We strongly believe that the best ideas and results come from collaboration – so let’s work together to bring your creative visions to life.</p>

                    <form action="{{url('contact')}}" method="post">
                        <input type="hidden" name="source" value="contact-page">

                        <div class="row">
                            <div class="col-md-12">
                                @if(Session::has('message'))
                                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @csrf

                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            </div>
                        </div>
                        
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Name" name="name" required>
                            </div>
                            <div class="col-md-6">
                                <input type="email" class="form-control" placeholder="Email" name="email" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Phone" name="phone" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Subject" name="subject" required>
                            </div>
                            <div class="col-md-12">
                                <textarea class="form-control" placeholder="Message" cols="5" name="message" required></textarea>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="seemore" value="Send">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection
