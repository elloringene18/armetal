@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/structure.css?v=<?php echo rand(1,999999); ?>">
@endsection

@section('content')
    <section id="structure">
        <div class="wrap container-fluid">
            <div class="row">
                <div class="col-md-4 left">
                    <div class="contv active slide-in-left">
                        <img src="{{ asset('') }}/img/structures/arts-active.png" class="mb-3">
                        <h2 class="mb-3">Arts & Sculpture</h2>
                        <p>The design and development of metal artworks is in our DNA. We work with creatives and designers visionaries to create monuments, metal sculptures, and art installations through a process of design consultancy, artistic exploration, and the application of both handicraft skills and modern manufacturing technologies.</p>
                    </div>
                    <div class="contv slide-in-left">
                        <img src="{{ asset('') }}/img/structures/ceilings-active.png" class="mb-3">
                        <h2 class="mb-3">Ceilings and screens</h2>
                        <p>Using precision engineering technologies like laser and water jet cutting machines, we can design and engineer extraordinarily intricate and decorative interior ceilings and screens. We work hand-in-glove collaborate directly with designers and developers to create stunning designs results for a wide range of environments, from hotel lobbies to airport lounges, royal rooms, and corporate headquarters.
                        </p>
                    </div>
                    <div class="contv slide-in-left">
                        <img src="{{ asset('') }}/img/structures/doors-active.png" class="mb-3">
                        <h2 class="mb-3">Doors & Windows</h2>
                        <p>From hospitals to malls, royal palaces, and airports, we have created some of the most exciting landmark entrances to an enormous range of environments in the Middle East. Our capabilities range from the design and manufacture of unglazed hollow core doors to sandblasted patterns, frameless fronts, and decorative patterns across a wide range of metals and materials.</p>
                    </div>
                    <div class="contv slide-in-left">
                        <img src="{{ asset('') }}/img/structures/exterior-active.png" class="mb-3">
                        <h2 class="mb-3">Exterior design</h2>
                        <p>We design and engineer intricate and extraordinary facades. By working hand-in-glove with architects and and construction visionaries, we   deliver bespoke, precision engineered exteriors that serve a commercial, cultural, aesthetic, and artistic purpose.
                        </p>
                    </div>
                    <div class="contv slide-in-left">
                        <img src="{{ asset('') }}/img/structures/claddings-active.png" class="mb-3">
                        <h2 class="mb-3">Cladding &
                            Curtain Wall Systems</h2>
                        <p>Our cladding, canopies, and curtain wall systems can be found right across the Middle East in large malls, arcades, courtyards, atriums, and canopies. We engage with designers and developers to create exquisite installations in a wide choice of materials, including aluminium, bronze, brass, stainless steel, glass, and composites.</p>
                    </div>
                    <div class="contv slide-in-left">
                        <img src="{{ asset('') }}/img/structures/stairs-active.png" class="mb-3">
                        <h2 class="mb-3">Stairways</h2>
                        <p>There is enormous potential to make architecturally extraordinary statements with stairways, balustrades, and handrails. Our work in the Middle East moves far beyond the functional, towards designs that make a powerful aesthetic impact in the construction sector.</p>
                    </div>
                    <div class="contv slide-in-left">
                        <img src="{{ asset('') }}/img/structures/balustrades-active.png" class="mb-3">
                        <h2 class="mb-3">Balustrades &
                            Handrails</h2>
                        <p>There is enormous potential to make architecturally extraordinary statements with stairways, balustrades, and handrails. Our work in the Middle East moves far beyond the functional, towards designs that make a powerful aesthetic impact in the construction sector.</p>
                    </div>
                </div>
                <div class="col-md-8 right" style="padding: 0">
                    <div id="right">
                        <div class="itm active"><div class="txt">Arts & Scuplture</div> <span class="icon arts"></div>
                        <div class="itm"><div class="txt">Ceilings and screens</div> <span class="icon ceilings"></div>
                        <div class="itm"><div class="txt">Doors & Windows</div> <span class="icon doors"></div>
                        <div class="itm"><div class="txt">Exterior design</div> <span class="icon exterior"></div>
                        <div class="itm"><div class="txt">Cladding & Curtain Wall Systems</div> <span class="icon cladding"></div>
                        <div class="itm"><div class="txt">Stairways</div> <span class="icon stairs"></div>
                        <div class="itm"><div class="txt">Balustrades & Handrails</div> <span class="icon balustrades"></div>
                    </div>
                    <div id="main">
                    </div>
                    <div id="side">
                    </div>
                    <div id="arrows">

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        var currectSlide = <?php echo isset($_GET['item']) ? $_GET['item'] : '0'; ?>;

        var images = [
            [
                '{{ asset('') }}/img/structure/arts/1.jpg',
                '{{ asset('') }}/img/structure/arts/2.jpg',
                '{{ asset('') }}/img/structure/arts/3.jpg',
                '{{ asset('') }}/img/structure/arts/4.jpg',
                '{{ asset('') }}/img/structure/arts/5.jpg',
                '{{ asset('') }}/img/structure/arts/6.jpg',
                '{{ asset('') }}/img/structure/arts/7.jpg',
                '{{ asset('') }}/img/structure/arts/8.jpg',
                '{{ asset('') }}/img/structure/arts/9.jpg',
                '{{ asset('') }}/img/structure/arts/10.jpg',
                '{{ asset('') }}/img/structure/arts/11.jpg',
                '{{ asset('') }}/img/structure/arts/12.jpg',
                '{{ asset('') }}/img/structure/arts/13.jpg',
            ],
            [
                '{{ asset('') }}/img/structure/ceilings/1.jpg',
                '{{ asset('') }}/img/structure/ceilings/2.jpg',
                '{{ asset('') }}/img/structure/ceilings/3.jpg',
                '{{ asset('') }}/img/structure/ceilings/4.jpg',
                '{{ asset('') }}/img/structure/ceilings/5.jpg',
                '{{ asset('') }}/img/structure/ceilings/6.jpg',
                '{{ asset('') }}/img/structure/ceilings/7.jpg',
                '{{ asset('') }}/img/structure/ceilings/8.jpg',
                '{{ asset('') }}/img/structure/ceilings/9.jpg',
            ],
            [
                '{{ asset('') }}/img/structure/doors/1.jpg',
                '{{ asset('') }}/img/structure/doors/2.jpg',
                '{{ asset('') }}/img/structure/doors/3.jpg',
                '{{ asset('') }}/img/structure/doors/4.jpg',
                '{{ asset('') }}/img/structure/doors/5.jpg',
                '{{ asset('') }}/img/structure/doors/1.jpg',
                '{{ asset('') }}/img/structure/doors/2.jpg',
                '{{ asset('') }}/img/structure/doors/3.jpg',
                '{{ asset('') }}/img/structure/doors/4.jpg',
                '{{ asset('') }}/img/structure/doors/5.jpg',
            ],
            [
                '{{ asset('') }}/img/structure/exterior/1.jpg',
                '{{ asset('') }}/img/structure/exterior/2.jpg',
                '{{ asset('') }}/img/structure/exterior/3.jpg',
                '{{ asset('') }}/img/structure/exterior/4.jpg',
                '{{ asset('') }}/img/structure/exterior/5.jpg',
                '{{ asset('') }}/img/structure/exterior/6.jpg',
                '{{ asset('') }}/img/structure/exterior/7.jpg',
                '{{ asset('') }}/img/structure/exterior/8.jpg',
                '{{ asset('') }}/img/structure/exterior/9.jpg',
                '{{ asset('') }}/img/structure/exterior/10.jpg',
                '{{ asset('') }}/img/structure/exterior/11.jpg',
                '{{ asset('') }}/img/structure/exterior/12.jpg',
                '{{ asset('') }}/img/structure/exterior/13.jpg',
                '{{ asset('') }}/img/structure/exterior/14.jpg',
                '{{ asset('') }}/img/structure/exterior/15.jpg',
                '{{ asset('') }}/img/structure/exterior/16.jpg',
            ],
            [
                '{{ asset('') }}/img/structure/cladding/1.jpg',
                '{{ asset('') }}/img/structure/cladding/2.jpg',
                '{{ asset('') }}/img/structure/cladding/3.jpg',
                '{{ asset('') }}/img/structure/cladding/4.jpg',
                '{{ asset('') }}/img/structure/cladding/5.jpg',
                '{{ asset('') }}/img/structure/cladding/6.jpg',
                '{{ asset('') }}/img/structure/cladding/7.jpg',
                '{{ asset('') }}/img/structure/cladding/8.jpg',
                '{{ asset('') }}/img/structure/cladding/9.jpg',
                '{{ asset('') }}/img/structure/cladding/10.jpg',
                '{{ asset('') }}/img/structure/cladding/11.jpg',
                '{{ asset('') }}/img/structure/cladding/12.jpg',
                '{{ asset('') }}/img/structure/cladding/13.jpg',
                '{{ asset('') }}/img/structure/cladding/14.jpg',
                '{{ asset('') }}/img/structure/cladding/15.jpg',
            ],
            [
                '{{ asset('') }}/img/structure/stairs/1.jpg',
                '{{ asset('') }}/img/structure/stairs/2.jpg',
                '{{ asset('') }}/img/structure/stairs/3.jpg',
                '{{ asset('') }}/img/structure/stairs/4.jpg',
                '{{ asset('') }}/img/structure/stairs/5.jpg',
                '{{ asset('') }}/img/structure/stairs/6.jpg',
                '{{ asset('') }}/img/structure/stairs/7.jpg',
                '{{ asset('') }}/img/structure/stairs/8.jpg',
            ],
            [
                '{{ asset('') }}/img/structure/balustrades/1.jpg',
                '{{ asset('') }}/img/structure/balustrades/2.jpg',
                '{{ asset('') }}/img/structure/balustrades/3.jpg',
                '{{ asset('') }}/img/structure/balustrades/4.jpg',
                '{{ asset('') }}/img/structure/balustrades/1.jpg',
                '{{ asset('') }}/img/structure/balustrades/2.jpg',
                '{{ asset('') }}/img/structure/balustrades/3.jpg',
                '{{ asset('') }}/img/structure/balustrades/4.jpg',
            ]
        ];

        function initSlides(index){

            $('.itm').removeClass('active');
            $('.contv').removeClass('active');

            $('.contv').eq(index).addClass('active');
            $('.itm').eq(index).addClass('active');

            $('#main').empty();
            $('#side').empty();

            for (let i = 0; i < images[index].length; i++) {
                $('#main').prepend('<div><img src="'+images[index][i]+'" width="100%"></div>');
                $('#side').prepend('<div><img src="'+images[index][i]+'" width="100%"></div>');
            }

            setTimeout(function (){
                $('#main').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    asNavFor: '#side'
                });
                $('#side').slick({
                    lazyLoad: 'ondemand',
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    asNavFor: '#main',
                    vertical: true,
                    verticalScrolling: true,
                    dots: false,
                    centerMode: false,
                    focusOnSelect: true,
                    arrows: true,
                    appendArrows: '#arrows',
                    variableHeight: true,
                    nextArrow: '<button class="slick-arrow slick-prev"></button>',
                    prevArrow: '<button class="slick-arrow slick-next"></button>',
                    responsive: [
                        {
                            breakpoint: 1180,
                            settings: {
                                slidesToShow: 5,
                                slidesToScroll: 1,
                                variableHeight: false,
                                vertical: false,
                                verticalScrolling: false,
                            }
                        }
                    ]
                });
            },300);
        }

        function uninitSlides(){
            $('#main').hide().slick("unslick");
            $('#side').hide().slick("unslick");
        }


        $('#right .itm').on('click',function (){

            uninitSlides();

            initSlides($('#right .itm').index(this));

            $('#right .itm').removeClass('active');
            $(this).addClass('active');

            indx = $('#right .itm').index(this);

            $('.contv').removeClass('active');
            $('.contv').eq(indx).addClass('active');

            $('#main').fadeIn(500);
            $('#side').fadeIn(500);
        });

        initSlides(currectSlide);

        setInterval(function (){console.log('refreshing');$('#side').slick('setPosition');},1000);
    </script>
@endsection
