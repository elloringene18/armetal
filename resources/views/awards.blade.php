@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/contact.css?v=<?php echo rand(1,999999); ?>">
    <link rel="stylesheet" href="{{ asset('') }}/css/awards.css?v=<?php echo rand(1,999999); ?>">
@endsection

@section('content')
    <section id="design">

        <!-- Modal -->
        <div class="modal fade" id="healthModal1" tabindex="-1" role="dialog" aria-labelledby="healthModal1" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="{{ asset('') }}/img/awards/health/OHSAS_18001.jpg" width="100%">
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="healthModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="{{ asset('') }}/img/awards/health/armetal_health_saftey_environmental_policy.gif" width="100%">
                    </div>
                </div>
            </div>
        </div>



        <div class="imgtxt">
            <div class="row">
                <div class="col-md-12 slide-in-left">
                    <h1 class="heading">QUALITY AND <br/>HEALTH & SAFETY</h1>
                    <p>Armetal is deeply committed to securing the health, safety and wellness of its employees,
                        and to meeting the highest standards of quality and environmental responsibility. </p>
                </div>
            </div>
        </div>


        <!-- Modal -->
        <div class="modal fade" id="qualityModal1" tabindex="-1" role="dialog" aria-labelledby="healthModal1" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="{{ asset('') }}/img/awards/quality/ISO_9001.jpg" width="100%">
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="qualityModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <img src="{{ asset('') }}/img/awards/quality/armetal_quality_policy.gif" width="100%">
                    </div>
                </div>
            </div>
        </div>

        <div class="wrap">
            <div class="boxes">
                <div class="box">
                    <img src="{{ asset('') }}/img/awards/18001.png">
                    <div class="conts slide-in-top-long">
                        <p>Armetal Metal Industires Co. is committed to operating in an environment where Quality is our way of doing business. Through the contributions of our employees, we as a team will guarantee excellence by continuously improving our performance.</p>
                        <p>We understand our customer’s needs and strive to exceed their expectations.</p>
                        <div class="row">
                            <div class="col-md-6"><a href="{{ asset('public/pdfs/CT-OHSAS18001-2007-UKAS-EN-A4-12.dec.17.pdf') }}" class="seemore" target="_blank">Safety Certificate</a></div>
                            <div class="col-md-6"><a href="{{ asset('public/pdfs/QHSE Policy.PDF') }}" class="seemore" target="_blank">Safety Policy</a></div>
                        </div>
                    </div>
                </div>
                <div class="box">
                    <img src="{{ asset('') }}/img/awards/9001.png">
                    <div class="conts slide-in-top-long">
                        <p>Armetal Metal Industires Co. is committed to operating in an environment where Quality is our way of doing business. Through the contributions of our employees, we as a team will guarantee excellence by continuously improving our performance.</p>
                        <p>We understand our customer’s needs and strive to exceed their expectations.</p>
                        <div class="row">
                            <div class="col-md-6"><a href="{{ asset('public/pdfs/CT-ISO 9001-2015-UKAS-EN-A4-P-13.dec.11.pdf') }}" class="seemore" target="_blank">Quality Certificate</a></div>
{{--                            <div class="col-md-6"><a href="#" class="seemore"  data-bs-toggle="modal" data-bs-target="#qualityModal2" >Quality Policy</a></div>--}}
                        </div>
                    </div>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="enviromentModal1" tabindex="-1" role="dialog" aria-labelledby="healthModal1" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="{{ asset('') }}/img/awards/environment/ISO_14001.jpg" width="100%">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Modal -->
                <div class="modal fade" id="enviromentModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <img src="{{ asset('') }}/img/awards/environment/armetal_health_saftey_environmental_policy.png" width="100%">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box">
                    <img src="{{ asset('') }}/img/awards/14001.png">
                    <div class="conts slide-in-top-long">
                        <div class="row pt-3">
                            <div class="col-md-6"><a href="{{ asset('public/pdfs/ISO 14001_2015-UKAS-EN-A4-12.dec.17.pdf') }}" class="seemore mt-5" target="_blank">Enviromental Certification</a></div>
{{--                            <div class="col-md-6"><a href="#" class="seemore mt-5" data-bs-toggle="modal" data-bs-target="#enviromentModal2">Enviromental Policy</a></div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="imgtxt">
            <div class="row">
                <div class="col-md-12 slide-in-left">
                    <p>All Armetal operations are governed according to the principles laid out in its ISO certifications, including ISO 9001 for Quality, ISO 14001 for the Environment and OHSAS 18001 for Health and Safety.</p>
                    <p>These certifications reflect our position as fierce proponents of ethical industry and the protection of the people we employ and the communities we serve.</p>
                </div>
            </div>
        </div>
        <br/>
        <div class="imgtxt">
            <div class="row">
                <div class="col-md-12 slide-in-left">
                    <h3>Privacy Policy</h3>
                    <p>
This website and its content is property of Armetal Metal Industries Company Ltd. 2015. All rights reserved. Any redistribution, reproduction or use of content or pictures on your of another website in part or all of the contents in any form is prohibited other than the following. You may print or download to a local hard disk & extract for your personal and non-commercial use only. You may copy the content to individual third parties for their personal use, but only if you acknowledge the website as the source of the material. You may not, except with our express written permission, distribute or commercially exploit the content. Nor may you transmit it or store it in any other website or other form of electronic retrieval system</p>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection

