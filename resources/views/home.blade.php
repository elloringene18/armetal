@extends('master')

@section('css')
@endsection

@section('content')
  <section id="homebanner">
    <span class="lines slideleftright"></span>
    <!-- <img src="img/homebanner.png" width="100%"> -->

    <video width="100%" loop muted autoplay playsinline>
      <source src="{{ asset('') }}/img/landing-video.mp4" type="video/mp4">
    Your browser does not support the video tag.
    </video>
  </section>

  <section id="intro">
    <div class="container">
      <div class="row">
        <div class="col-md-5 relative">
          <img src="{{ asset('') }}/img/home-cube.png" id="homecube" width="100%">
        </div>
        <div class="col-md-7 copy">
          <h3 class="heading">Prestige through precision</h3>

          <p>It is our impassioned belief that every building, structure, and artwork is a statement of intent.</p>
          <p>Every product that leaves an Armetal workshop and factory is precision-engineered through meticulous design and attention to detail. Our work proudly illuminates the iconic landscape of Saudi Arabia.</p>
          <p>We achieve this by working in close partnership with architects, designers and construction innovators. By combining the precision of automated fabrication technologies with artisanal vision, we fuse artistic intent with structural ingenuity.</p>
          <p>As a proud champion of Saudi manufacturing excellence, we ensure that Saudi engineering remains at the forefront of industrial development and support the achievements of the ambitious regional vision.</p>

          <a href="{{ url('/about') }}" class="seemore">Discover more</a>
        </div>
      </div>
    </div>
  </section>

  <section id="history" class="clearfix">
    <div class="container">
      <div class="relative clearfix">
        <div class="left">
          <div id="mainCar">
            <div><img src="{{ asset('') }}/img/homeprojects/main1.jpg" width="100%"> <span class="date">19<br/>85</span></div>
            <div><img src="{{ asset('') }}/img/homeprojects/main2.jpg" width="100%"> <span class="date">19<br/>86</span></div>
            <div><img src="{{ asset('') }}/img/homeprojects/main3.jpg" width="100%"> <span class="date">19<br/>87</span></div>
            <div><img src="{{ asset('') }}/img/homeprojects/main4.jpg" width="100%"> <span class="date">19<br/>88</span></div>
            <div><img src="{{ asset('') }}/img/homeprojects/main5.jpg" width="100%"> <span class="date">19<br/>89</span></div>
            <div><img src="{{ asset('') }}/img/homeprojects/main6.jpg" width="100%"> <span class="date">19<br/>90</span></div>
          </div>
        </div>
        <div class="right">

          <div class="copy">
            <h1 class="head">Our History</h1>
            <p>By meeting the challenges of metal architectural visionaries, Armetal ensures that Saudi engineering remains at the forefront of industrial development and illuminates the accomplishments of the ambitious regional expansion.</p>
            <p>It is our impassioned belief that every building, structure, and artwork is a statement of purpose. That is how Armetal has sculpted some of the Middle East’s most iconic skylines.</p>
            <a href="{{ url('/about') }}" class="seemore">See more</a>
          </div>

          <div id="sideCar">
            <div><img src="{{ asset('') }}/img/homeprojects/nav1.jpg" width="100%"></div>
            <div><img src="{{ asset('') }}/img/homeprojects/nav2.jpg" width="100%"></div>
            <div><img src="{{ asset('') }}/img/homeprojects/nav3.jpg" width="100%"></div>
            <div><img src="{{ asset('') }}/img/homeprojects/nav4.jpg" width="100%"></div>
            <div><img src="{{ asset('') }}/img/homeprojects/nav5.jpg" width="100%"></div>
            <div><img src="{{ asset('') }}/img/homeprojects/nav6.jpg" width="100%"></div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@section('js')
@endsection
