@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('') }}/css/structures.css?v=<?php echo rand(1,999999); ?>">
@endsection

@section('content')
<section id="diagram">
    <div class="container clearfix">
        <div class="copy">
            <div class="row">
                <div class="slide-in-top">
                    <h1 class="heading">STRUCTURES</h1>
                    <p>Collaboration is the key to unlocking design innovation, which is why Armetal has so successfully completed an exceptionally
                        diverse range of projects across multiple sectors -construction, architecture, urban planning and public art.</p>
                </div>
            </div>
        </div>
        <div id="cube" class="slide-in-bottom">
            <img src="{{ asset('') }}/img/structures/cube2.png" class="cube">
            <ul class="left slide-in-left">
                <li>
                    <span>Arts & Sculpture</span><a href="{{ url('/structure?item=0') }}"><span class="icon arts"></span></a>
{{--                    <div class="line line-1 rotate-in-2-cw">--}}
{{--                        <div class="line line-2 rotate-in-2-cw">--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </li>
                <li>
                    <span>Ceilings and screens</span> <a href="{{ url('/structure?item=1') }}"><span class="icon ceilings"></span></a>

{{--                    <div class="line line-3 rotate-in-2-cw">--}}
{{--                        <div class="line line-4 rotate-in-2-cw">--}}
{{--                            <div class="line line-5 rotate-in-2-cw">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </li>
                <li>
                    <span>Doors & Windows</span> <a href="{{ url('/structure?item=2') }}"><span class="icon doors"></span></a>

{{--                    <div class="line line-6 rotate-in-2-cw">--}}
{{--                    </div>--}}
                </li>
                <li>
                    <span>Exterior design</span> <a href="{{ url('/structure?item=3') }}"><span class="icon exterior"></span></a>

{{--                    <div class="line line-7 rotate-in-2-cw">--}}
{{--                        <div class="line line-8 rotate-in-2-cw">--}}
{{--                            <div class="line line-9 rotate-in-2-cw">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                </li>
            </ul>
            <ul class="right slide-in-right">
                <li><a href="{{ url('/structure?item=4') }}">
                        <span class="icon cladding"></span></a>  <span>Cladding &<br/>Curtain Wall Systems</span>
{{--                    <div class="line line-10 rotate-in-2-cw">--}}
{{--                    </div>--}}
                </li>
                <li><a href="{{ url('/structure?item=5') }}"><span class="icon stairs"></span></a>  <span>Stairways</span>
{{--                    <div class="line line-11 rotate-in-2-cw">--}}
{{--                    </div>--}}
                </li>
                <li><a href="{{ url('/structure?item=6') }}"><span class="icon balustrades"></span></a>  <span>Balustrades &<br/> Handrails</span>
{{--                    <div class="line line-11 rotate-in-2-cw">--}}
{{--                    </div>--}}
                </li>
            </ul>
        </div>
    </div>
</section>

<section id="structvideo">
    <div class="container-fluid">   
        <div class="row">
            <div class="col-lg-7 col-md-12 left">
                <video width="100%"  autoplay muted playsinline loop>
                  <source src="{{ asset('') }}/img/structures/1-opt.mp4" type="video/mp4">
                Your browser does not support the video tag.
                </video>
                
            </div>
            <div class="col-lg-5 col-md-12 right">
                <h1 class="heading">The design advantage</h1>
                <p>By unleashing creative freedom across our business, our customers can see their visions come to life whilst retaining influence throughout the design and manufacturing journey: true partnerships that deliver.</p>
                <p>We maintain a competitive edge and creative breadth by continuing to be the only manufacturer in the Middle East that designs and produces custom-engineered steel and special alloy ornamental decorative pipes and tubes. Our product lines are an indicative – but not exhaustive – demonstration of the work we do.</p>
            </div>
        </div>
    </div>
</section>

<section id="projects">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="heading">Our projects</h1>

                <div class="tabs">
                    <div class="tab active">Kingdom of Saudi Arabia</div>
                    <div class="tab">Outside the kingdom</div>
                </div>

                <div class="tabcontents">
                    <div class="content active">
                        <div class="table">
                            <div class="trow">
                                <div class="tcol head">Name of Project</div>
                                <div class="tcol head">location</div>
                                <div class="tcol head">Main contractor</div>
                                <div class="tcol head">consultant</div>
                                <div class="tcol head">Products applied</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al-Khobar Mall Plaza</div>
                                <div class="tcol ">Al Khobar</div>
                                <div class="tcol ">Hussein Saklou (owner)</div>
                                <div class="tcol ">Jamjoom Consult</div>
                                <div class="tcol ">SST Handrail, Curtain Wall Cladding, Escalator, Column, Showcase, Canopy</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Aramco Office Building</div>
                                <div class="tcol ">Tanajib</div>
                                <div class="tcol ">Al Osais Contracting Est.</div>
                                <div class="tcol ">Saudi Aramco</div>
                                <div class="tcol ">Balustrade, Curtain Wall, Skylight</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Core Area Annex Building</div>
                                <div class="tcol ">Dhahran</div>
                                <div class="tcol ">Kettaneh Bros.</div>
                                <div class="tcol ">Saudi Aramco</div>
                                <div class="tcol ">Main Entrance Canopy, Column Cladding</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Apicorp Head Office Building</div>
                                <div class="tcol ">Al Khobar</div>
                                <div class="tcol ">Saudi Binladin Group</div>
                                <div class="tcol ">DEGW London Limited</div>
                                <div class="tcol ">SST &amp; Mild Steel Handrails</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Jumah Commercial Center</div>
                                <div class="tcol ">Dammam</div>
                                <div class="tcol ">Al Osais Cont. Est.</div>
                                <div class="tcol ">Buro Happold</div>
                                <div class="tcol ">Miscellaneous Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Rashid Shopping Mall</div>
                                <div class="tcol ">Al Khobar</div>
                                <div class="tcol ">Al Rashid Trading &amp; Cont.</div>
                                <div class="tcol ">Al Rashid Trading &amp; Cont.</div>
                                <div class="tcol ">SST Glass Windows, Doors / Curtain Walls / SST Column Cladding / SST Staircase.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">King Fahd International Airport</div>
                                <div class="tcol ">Dammam</div>
                                <div class="tcol ">Various Contractors</div>
                                <div class="tcol ">Saudi Arabian Bechtel Co.</div>
                                <div class="tcol ">Miscellaneous Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Madinah Hilton Hotel</div>
                                <div class="tcol ">Jeddah</div>
                                <div class="tcol ">Haif Trading &amp; Contg. Est.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST/Brass/Glass Balustrade</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">King Abdulaziz University</div>
                                <div class="tcol ">Jeddah</div>
                                <div class="tcol ">Cercon</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Balustrade</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Madinah Road Shopping Center</div>
                                <div class="tcol ">Jeddah</div>
                                <div class="tcol ">Saudi Binladin Group</div>
                                <div class="tcol ">Saudi Diyar Consulting</div>
                                <div class="tcol ">Miscellaneous Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Equestrian Club Project</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Saudi  Oger  Ltd.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST  Handrails</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Kingdom Center</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">El Seif Engineering</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Various SST Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Saudi Arabian Monetary Agency</div>
                                <div class="tcol ">Various Sites</div>
                                <div class="tcol ">Dumez</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Miscellaneous Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Yamamah Facilities</div>
                                <div class="tcol ">Various Sites</div>
                                <div class="tcol ">J &amp; P Overseas Ltd.</div>
                                <div class="tcol ">British Aerospace</div>
                                <div class="tcol ">Miscellaneous Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Faisaliah Center</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Saudi Binladin Group</div>
                                <div class="tcol ">Norman Foster &amp; Partners and Buro Happold J.V.</div>
                                <div class="tcol ">Various Stainless Steel Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">King Khaled International Airport</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Saudi Oger Ltd.</div>
                                <div class="tcol ">KKIA Facility Engineering</div>
                                <div class="tcol ">Partition</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Khayma Center</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Al Saad Contracting Co.</div>
                                <div class="tcol ">Saudi Diyar Consultant</div>
                                <div class="tcol ">Stainless Steel / Glass Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Pricerite</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Abdul Rahman Saad Al Rashid</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Aluminum Cladding &amp; Canopy</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">L’etoile Restaurant</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">National Construction Group (NCG)</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Stainless Steel Decorative Arch Canopy</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Centria Center</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">First Saudi Contracting Co.</div>
                                <div class="tcol ">Projacs</div>
                                <div class="tcol ">SST Canopies / Aluminum Roof Top Wall</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">STC / Al Jawal Head Quarters Building</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Swayeh Co.</div>
                                <div class="tcol ">Delta Engineering</div>
                                <div class="tcol ">Stainless Steel Canopies / Walkway Shelter / SST Balustrade.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Dallah Hospital Expansion</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Hashem Cont. &amp; Trad. Co.</div>
                                <div class="tcol ">Projacs</div>
                                <div class="tcol ">SST Truss / SST Decorative Stair / SST Frames &amp; Doors / SST Handrails.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Auto Auction Yard</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Swayeh Co.</div>
                                <div class="tcol ">Zuhair Fayez &amp; Associates</div>
                                <div class="tcol ">SST Column Cladding / Aluminum Canopies / SST Flag Poles / SST Spheres.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Residential &amp; Recreational Complex - ARAMCO</div>
                                <div class="tcol ">Khurais</div>
                                <div class="tcol ">Al Osais</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Cladding / SST Balustrade</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Rashid Mega Mall - Madinah</div>
                                <div class="tcol ">Madinah</div>
                                <div class="tcol ">Abdul Rahman Saad Al-Rashid</div>
                                <div class="tcol ">A.R.T.A.R</div>
                                <div class="tcol ">SST &amp; Aluminum Cladding Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Toyota Showroom</div>
                                <div class="tcol ">Jubail</div>
                                <div class="tcol ">Project Build</div>
                                <div class="tcol ">Al Mutlaq - Bu Nuhaya</div>
                                <div class="tcol ">SST Handrail / Aluminum Cladding</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Rashed Tower</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">R.T.C.C</div>
                                <div class="tcol ">R.T.C.C</div>
                                <div class="tcol ">SST &amp; Glass Balustrade, SST Column Cladding</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Anoud Tower</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Al Redwan </div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Decorative Staircase / Glass Balustrade</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Shk. Ibrahim Al Saedan Office Bldg.</div>
                                <div class="tcol ">Riyadh </div>
                                <div class="tcol ">Al Arrab Contracting.</div>
                                <div class="tcol ">Al Wasat Consulting</div>
                                <div class="tcol ">SST Decorative Columns / Structure at Façade with Crown and Logo</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Rajhi Bank</div>
                                <div class="tcol ">Various Sites</div>
                                <div class="tcol ">Al Dossary Advertising</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Aluminum Composite Cladding Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Riyadh Bank - ATM Canopy</div>
                                <div class="tcol ">Various Sites</div>
                                <div class="tcol ">Al Dossary Advertising</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Aluminum Composite Cladding Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Riyadh Bank Drive - up ATM ( Island)</div>
                                <div class="tcol ">Various Sites</div>
                                <div class="tcol ">Al Dossary Advertising</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Aluminum Kiosk, Enclosure, Pylon &amp; Canopy</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Riyadh Bank Drive - up ATM</div>
                                <div class="tcol ">Various Sites</div>
                                <div class="tcol ">Al Dossary Advertising</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Aluminum Kiosk, Pylon &amp; Canopy</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Air Force HQ</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Saudi Build</div>
                                <div class="tcol ">Projacs</div>
                                <div class="tcol ">Mild Steel Structure Canopy Cladded with Aluminum Composite Panel.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Saedan Office Bldg.</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Saudi Build</div>
                                <div class="tcol ">Al Wasat Consulting</div>
                                <div class="tcol ">SST Handrails, SST Cladding. </div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Omar Furniture</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Hashem Contracting</div>
                                <div class="tcol ">M. M. Al Sabbagh  </div>
                                <div class="tcol ">SST Handrails, Glass/SST Bridges, Elevator Cladding.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Samba Bank - ATM</div>
                                <div class="tcol ">Various Sites </div>
                                <div class="tcol ">SAMBA Financial Group</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Stainless Steel Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Samama Complex</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Samama Contracting</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Aluminum Canopies, SST Handrails. </div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">National Commercial Bank (NCB) Headquarters</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Nesma &amp; Partners</div>
                                <div class="tcol ">Zuhair Fayez &amp; Associates</div>
                                <div class="tcol ">SST Handrail, Glass Balustrade, SST Ceiling &amp; Canopy</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Magrabi Hospital</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Mamco</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Handrails, SST Grills, Column Cladding.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Dallah Hospital</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Hashem Contracting</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Stainless Steel Mesh Handrails,  Stainless Steel Doors &amp; Door Jambs</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Rashid Tower</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">RTCC</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Glass Curtain Wall, Doors, SST handrail &amp; Glass Balustrade</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Faisal University</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">AZRB</div>
                                <div class="tcol ">Buro Happold</div>
                                <div class="tcol ">SST &amp; Glass Balustrade / Glass Screens / Steel Handrail</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Ajlan Tower</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Triplicity House CO.</div>
                                <div class="tcol ">Al Mutlaq - Bu Nuhaya</div>
                                <div class="tcol ">Aluminum Composite Cladding, Canopies, SST Decorative Works.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">King Abdallah University for Science and Technology (KAUST)</div>
                                <div class="tcol ">Thuwal</div>
                                <div class="tcol ">Saudi Oger</div>
                                <div class="tcol ">Saudi Aramco</div>
                                <div class="tcol ">SST Wall Cladding, Mild Steel Structural Bridges, SST Handrails, Canopies, Screens, GKD Mesh, Floor Strips, Zinc Cladding</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Tamkeen Tower</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">East &amp; West</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Canopy</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Moussa Tower</div>
                                <div class="tcol ">Dammam</div>
                                <div class="tcol ">Alugate</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Canopy</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Marina</div>
                                <div class="tcol ">Obhur</div>
                                <div class="tcol ">Fressinet</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Handrail, Glass Balustrade, SST Wall Cladding</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Princess Noura University (PNU)</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Saudi Oger</div>
                                <div class="tcol ">Dar Al Handasah</div>
                                <div class="tcol ">SST Handrails, Glass Balustrade, SST Column &amp; Wall Cladding, SST Column Cladding, Mild Steel with Acrylic Sheet</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Jeraisy Furniture</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Al Jeraisy</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Handrail, Glass Balustrade, Glass Curtain Wall &amp; Doors, Aluminum Canopy &amp; Wall Cladding</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Riyadh Airbase</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Saudi Lebanese Tarouk</div>
                                <div class="tcol ">Riyadh Airbase</div>
                                <div class="tcol ">Aluminum Composite Cladding for Canopy</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">King Khaled International Airport</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">SATCO</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST/Glass Doors, Aluminum Mashrabiya, Aluminum Cladding, SST Handrails/Guardrails</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Bahar Villa</div>
                                <div class="tcol ">Kuwait</div>
                                <div class="tcol ">Al Muhallab Trading &amp; Co.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Handrails, SST Staircase, Canopy, Glass Doors, Glass Partitions </div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Jizan Mall</div>
                                <div class="tcol ">Jizan </div>
                                <div class="tcol ">Abdul Rahman Saad EL Rashid</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Aluminum Wall Cladding</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Maraii Cradle</div>
                                <div class="tcol ">Kharj</div>
                                <div class="tcol ">Saudi Amana</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Handrail, Glass Balustrade, SST Grating &amp; Trench</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Makkah Clock Royal Tower</div>
                                <div class="tcol ">Makkah</div>
                                <div class="tcol ">Saudi BinLadin Group</div>
                                <div class="tcol ">Dar Al Handasah</div>
                                <div class="tcol ">SST Column Cladding &amp; SST/Glass Partitions and Screens</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Dallah Hospital (Phase III)</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Hashem Contracting</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Stainless Steel Mesh Handrails,  Stainless Steel Doors &amp; Door Jambs</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">King Saud University</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Saudi BinLaden Group</div>
                                <div class="tcol ">Saudi Consult Center &amp; Ranhill</div>
                                <div class="tcol ">Stainless Steel Handrails and Glass Balustrades</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">King Abdullah Petroleum and Studies Research Center (KAPSARC)</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">SK Engineering and Contracting</div>
                                <div class="tcol ">Saudi Aramco</div>
                                <div class="tcol ">SST Cladding, Canopies, Handrails, Coping, Bird Nest</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Olaya Towers</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Nesma &amp; Partners</div>
                                <div class="tcol ">Zuhair Fayez &amp; Partners</div>
                                <div class="tcol ">SST Strips, Frames, and Ladders</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Faisal University</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">ABV Rock Group</div>
                                <div class="tcol ">Buro Happold</div>
                                <div class="tcol ">SST/MS/Glass Handrails and Screens</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Riyadh Bank Branches</div>
                                <div class="tcol ">Various Sites</div>
                                <div class="tcol ">Riyadh Bank</div>
                                <div class="tcol ">Riyadh Bank</div>
                                <div class="tcol ">Counters, Canopies, Enclosures, Column Cladding, Display Unit.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Turaif Reception Center</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Freyssinet Saudi Arabia</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Copper Cladding &amp; Roofing System</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">King Abdullah Intl. Conference Center</div>
                                <div class="tcol ">Jeddah</div>
                                <div class="tcol ">Saudi Oger</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Internal Glazing Mirrors</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Ministry of Education (MOE)</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Saudi Oger</div>
                                <div class="tcol ">Projacs</div>
                                <div class="tcol ">Stainless Steel / Glass Balustrades</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Saudi Railway Passenger Station</div>
                                <div class="tcol ">Various Sites</div>
                                <div class="tcol ">Rashid Trading &amp; Contracting Co.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Stainless Steel Column Cladding, Canopy, Global Earth Feature and Waiting Area Glass Partitions</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Information Technology Communications Complex (ITCC)</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">J &amp; P (Overseas)</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Stainless Steel Handrails and Balustrades / Glass Partitions/ Stainless Steel Cladding, Mild Steel Stairs.</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">King Abdullah Petroleum and Studies Research Center (KAPSARC)</div>
                                <div class="tcol ">Riyadh</div>
                                <div class="tcol ">Drake &amp; Scull International</div>
                                <div class="tcol ">Saudi Aramco</div>
                                <div class="tcol ">Aluminum Composite Cladding</div>
                            </div>
                        </div>
                    </div>
                    <div class="content">
                        <div class="table">
                            <div class="trow">
                                <div class="tcol head">Name of Project</div>
                                <div class="tcol head">location</div>
                                <div class="tcol head">Main contractor</div>
                                <div class="tcol head">consultant</div>
                                <div class="tcol head">Products applied</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Al Wakra Hospital</div>
                                <div class="tcol ">Qatar</div>
                                <div class="tcol ">J &amp; P  (Overseas)  Ltd.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Aluminum Cladding, SST Handrails.</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Al Jawhara Commercial Center</div>
                                <div class="tcol ">Kuwait</div>
                                <div class="tcol ">Al Muhalab Contracting &amp; Trading</div>
                                <div class="tcol "></div>
                                <div class="tcol ">SST Architrave / SST Handrails / SST Sphere</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Al Maidan Hospital</div>
                                <div class="tcol ">Kuwait</div>
                                <div class="tcol ">Al Muhalab Contracting &amp; Trading</div>
                                <div class="tcol ">Gulf Consulting</div>
                                <div class="tcol ">SST Facia Cladding / SST Balustrade / SST Shelves</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Scientific Center</div>
                                <div class="tcol ">Kuwait</div>
                                <div class="tcol ">Al Muhalab Contracting</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Various SST Works</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Kharafi Center</div>
                                <div class="tcol ">Kuwait</div>
                                <div class="tcol ">Al Muhalab Contracting</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Various SST Works</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Doha Interon Hotel </div>
                                <div class="tcol ">Qatar</div>
                                <div class="tcol ">J &amp; P  (Overseas)  Ltd.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Various  SST  Works</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Royal Plaza at Al Sadd Street</div>
                                <div class="tcol ">Qatar</div>
                                <div class="tcol ">Contraco W.L.L</div>
                                <div class="tcol ">Salam inter consult</div>
                                <div class="tcol ">SST Decorative Handrail  Works</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Saudi Arabian Embassy</div>
                                <div class="tcol ">Qatar</div>
                                <div class="tcol ">Saudi Embassy</div>
                                <div class="tcol ">Sreco</div>
                                <div class="tcol ">Balustrade</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">AIC Headquarters Building</div>
                                <div class="tcol ">Kuwait</div>
                                <div class="tcol ">Al Muhallab Trading &amp; Contracting</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Stair, Balustrade</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Kuwait Trade Center</div>
                                <div class="tcol ">Kuwait</div>
                                <div class="tcol ">Al Muhalab Cont. &amp; Trad. Co.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Stainless Steel Works</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Bayan Palace</div>
                                <div class="tcol ">Kuwait</div>
                                <div class="tcol ">Al Muhalab Cont. &amp; Trad. Co.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Ornamental Decorative doors / Brass Works.</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">ETISALAT  -  Ajman</div>
                                <div class="tcol ">U.A.E.</div>
                                <div class="tcol ">Energoprojekt M.E.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Various SST Works</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">ETISALAT  -  Abu  Dhabi</div>
                                <div class="tcol ">U.A.E.</div>
                                <div class="tcol ">Arabian Const. Co.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Various SST Works</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">ETISALAT  -  Al  Ain</div>
                                <div class="tcol ">U.A.E.</div>
                                <div class="tcol ">Arabian Const. Co.</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Various SST Works</div>
                            </div>
                            <div class="trow">

                                <div class="tcol ">Etisalat Administration Building</div>
                                <div class="tcol ">Sharjah</div>
                                <div class="tcol ">Arabian Construction Co.</div>
                                <div class="tcol ">Consult</div>
                                <div class="tcol ">Miscellaneous Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">National Drilling Co. Headquarters</div>
                                <div class="tcol ">Abu Dhabi</div>
                                <div class="tcol ">Al Habtoor Engg. Ent.</div>
                                <div class="tcol ">Adnoc</div>
                                <div class="tcol ">Miscellaneous Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Madinah Souk</div>
                                <div class="tcol ">Dubai</div>
                                <div class="tcol ">Al Ghurair Group</div>
                                <div class="tcol ">SPP</div>
                                <div class="tcol ">Balustrade, Elevator Enclosure</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">National Bank of Abudhabi</div>
                                <div class="tcol ">UAE</div>
                                <div class="tcol ">Al Habtoor Engineering</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Various SST Works</div>
                            </div>
                            <div class="trow">
                                <div class="tcol ">Etisalat Customer Service Bldg.</div>
                                <div class="tcol ">UAE</div>
                                <div class="tcol ">Al Hamed Development</div>
                                <div class="tcol "></div>
                                <div class="tcol ">Various SST Works</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
@endsection


