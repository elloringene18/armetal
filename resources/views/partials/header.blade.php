<!doctype html>
<html class="no-js" lang="">

<head>
  <meta charset="utf-8">
  <title>Armetal - Metal Industries Co. Ltd</title>
  <meta name="description" content="Armetal Metal Industries Co. Ltd. was established in 1985 pursuant to the rapid development programs of the Kingdom of Saudi Arabia.">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:title" content="Armetal - Metal Industries Co. Ltd">
  <meta property="og:type" content="">
  <meta property="og:url" content="">
  <meta property="og:image" content="">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="{{ asset('') }}/css/normalize.css">
  <link rel="stylesheet" href="{{ asset('') }}/css/bootstrap.min.css">
  <link rel="stylesheet" href="{{ asset('') }}/fonts/stylesheet.css">
  <link rel="stylesheet" href="{{ asset('') }}/fonts/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>

  <link rel="stylesheet" href="{{ asset('') }}/css/main.css?v=<?php echo rand(1,999999); ?>">

  <meta name="theme-color" content="#fafafa">

  <link rel="icon" type="image/x-icon" href="{{ asset('') }}/img/favicon.png">


    @yield('css')
</head>

<body>

<div class="row" id="toptexts">
  <span></span>
</div>

<header id="header" class="">
  <div class="container">
    <div class="row">
      <div class="col-6">
        <a href="{{ url('/') }}" id="mainlogo"></a>
      </div>
      <div class="col-6">
        <div id="menubtcontainer" onclick="toggleMenu(this)">
          <div class="bar bar1"></div>
          <div class="bar bar2"></div>
          <div class="bar bar3"></div>
        </div>
      </div>
    </div>
    <div id="mainMenu" class="slide-in-top">
      <div class="wrap relative">
        <div class="lines slide-in-left2"></div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="container relative">
            <ul>
              <li><a href="{{ url('/') }}">Home</a></li>
              <li><a href="{{ url('/about') }}">Who we are</a></li>
              <li><a href="{{ url('/structures') }}">STRUCTURES</a></li>
              <li><a href="{{ url('/awards') }}">DESIGNED FOR EXCELLENCE</a></li>
              <li><a href="{{ url('/contact') }}">GET IN TOUCH</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
