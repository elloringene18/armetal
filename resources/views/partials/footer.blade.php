
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div id="footerlogo">
          <img src="{{ asset('') }}/img/logo-footer.png" width="" id="">
        </div>
        <br/>
        <span class="copyright">Copyright © 2021 - Armetal Metal Industries Company Ltd</span>
      </div>
      <div class="col-md-5">
        <ul>
          <li>
            <a href="{{ url('/about') }}">About us</a>
          </li>
          <li>
            <a href="{{ url('/structures') }}">Structures</a>
          </li>
          <li>
            <a href="{{ url('/awards') }}">Design for excellence</a>
          </li>
        </ul>
      </div>
      <div class="col-md-3">
        <div class="socials">
          <a href="https://www.linkedin.com/company/al-hejailan-group/" class="fa fa-linkedin" target="_blank"></a>
{{--          <a href="#" class="fa fa-twitter" target="_blank"></a>--}}
        </div>
        <div class="tiny-links">
          {{-- <a href="#">Terms of use</a> --}}
          <a href="{{ url('/awards') }}">Privacy policy</a>
        </div>
      </div>
    </div>
  </div>
</footer>

<script src="{{ asset('') }}/js/vendor/modernizr-3.11.2.min.js"></script>
<script
  src="https://code.jquery.com/jquery-3.6.0.min.js"
  integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4="
  crossorigin="anonymous"></script>
<script src="{{ asset('') }}/js/bootstrap.min.js"></script>
<script src="{{ asset('') }}/js/plugins.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="{{ asset('') }}/js/main.js?v=<?php echo rand(1,999999); ?>"></script>

<script type="text/javascript">
  $('#mainCar').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: '#sideCar'
  });
  $('#sideCar').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '#mainCar',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    initialSlide: 1,
    arrows: true,
    nextArrow: '<button class="slick-arrow slick-prev"></button>',
    prevArrow: '<button class="slick-arrow slick-next"></button>'
  });

  $(window).resize(function (){
    setTimeout(function (){
      $('#side').slick('setPosition');
      $('#main').slick('setPosition');
    },300)
  })
</script>
<!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
<script>
  window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
  ga('create', 'UA-XXXXX-Y', 'auto'); ga('set', 'anonymizeIp', true); ga('set', 'transport', 'beacon'); ga('send', 'pageview')
</script>
<script src="https://www.google-analytics.com/analytics.js" async></script>

@yield('js')
</body>

</html>
