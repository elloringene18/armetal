<?php

namespace App\Http\Controllers;

use App\Models\ContactEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class FormController extends Controller
{
    public function __construct()
    {
    }

    public function contact(Request $request){

//        $honeypot = $request->input('user_id');
//
//        if($honeypot)
//            return redirect()->back();

        $type = $request->input('source');

        $request->validate([
            'phone' => 'required|max:255',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'message' => 'required|max:1000',
        ]);

        $input = $request->except('_token','user_id','lang');

        $entry = ContactEntry::create(['ip'=>$request->ip(),'source'=>$type]);

        if($entry){

            foreach ($input as $key=>$item){
                $entry->items()->create(['key'=>$key,'value'=>$item]);
            }

            Session::flash('message','Thank you for your contacting us. We will get back to you soon.');

            $emailData = $input;

            $subject = 'Armetal Website - Contact Form';

            try {
                Mail::send('mail.contact', ['data' => $emailData], function ($message) use ($subject) {
                    $message->from('contactus@armetal.com', 'Armetal Website')->to('contactus@armetal.com', 'Admin')->subject($subject);
//                $message->from('info@ajmantourism.ae', $input['name'])->to('info@ajmantourism.ae', 'Ajman Tourism')->subject($subject);
                });
            }
            catch (\Exception $e) {
                dd($e);
            }

            return redirect()->back();
        }


        Session::flash('error','An error has occurred. Please try again later.');

        return redirect()->back();
    }
}
