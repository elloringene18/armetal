<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});


Route::get('admin/login', 'Admin\AuthController@index');
Route::post('admin/login', 'Admin\AuthController@customLogin');
Route::post('/contact','FormController@contact');

Route::group(['prefix' => 'admin','middleware' => ['auth']], function() {
    Route::get('/', function () {
        redirect('admin/contacts');
    });

    Route::group(['prefix' => 'contacts'], function() {
        Route::get('/','Admin\ContactController@index');
        Route::get('/export','Admin\ContactController@export');
        Route::get('/view/{id}','Admin\ContactController@view');
        Route::get('/delete/{id}','Admin\ContactController@delete');
    });

    Route::get('/{page}', function () {
        return view('404');
    });
});

Route::get('/{page}', function ($page) {
    $lang = 'en';
    if(view()->exists($page)){
        return view($page,compact('lang'));
    }
    return view('404');
});
